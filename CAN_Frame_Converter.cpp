#include <iostream>
#include <string.h>
#include <stdlib.h>

using namespace std;

// Function to convert hexadecimal to decimal
// Courtesy of https://www.geeksforgeeks.org/program-hexadecimal-decimal/ with minor change of char * to string
int hexadecimalToDecimal(string hexVal)
{   
    int len = hexVal.length();
     
    // Initializing base value to 1, i.e 16^0
    int base = 1;
     
    int dec_val = 0;
     
    // Extracting characters as digits from last character
    for (int i=len-1; i>=0; i--)
    {   
        // if character lies in '0'-'9', converting 
        // it to integral 0-9 by subtracting 48 from
        // ASCII value.
        if (hexVal[i]>='0' && hexVal[i]<='9')
        {
            dec_val += (hexVal[i] - 48)*base;
                 
            // incrementing base by power
            base = base * 16;
        }
 
        // if character lies in 'A'-'F' , converting 
        // it to integral 10 - 15 by subtracting 55 
        // from ASCII value
        else if (hexVal[i]>='A' && hexVal[i]<='F')
        {
            dec_val += (hexVal[i] - 55)*base;
         
            // incrementing base by power
            base = base*16;
        }
    }
     
    return dec_val;
}

int *CANtoInt(string rawCanFrame) {

    // For each of these we are first extracting the part of the CAN frame that corresponds to each value,
    // ordering it correctly 2 low bytes followed by 2 high bytes, and then converting that hex number to decimal
    int firstValue = hexadecimalToDecimal(rawCanFrame.substr(36,2) + rawCanFrame.substr(34,2));
    int secondValue = hexadecimalToDecimal(rawCanFrame.substr(40,2) + rawCanFrame.substr(38,2));
    int thirdValue = hexadecimalToDecimal(rawCanFrame.substr(44,2) + rawCanFrame.substr(42,2));
    int fourthValue = hexadecimalToDecimal(rawCanFrame.substr(48,2) + rawCanFrame.substr(46,2));

    //cout << firstValue << endl;
    //cout << secondValue << endl;
    //cout << thirdValue << endl;
    //cout << fourthValue << endl;

    static int returnValues[4];
    //taking out the part of the string we want (the first values worth in hex)
    returnValues[0] = firstValue;
    returnValues[1] = secondValue;
    returnValues[2] = thirdValue;
    returnValues[3] = fourthValue;
    return returnValues;
}


int main(int argc, char const *argv[]) {
    string c = "(1535072148.663651) can0 0CFFF048#0101000200030004"; // For reference, # is the 33rd character (starting from 0)
    int* rets;
    rets = CANtoInt(c);
    cout << *rets << endl;
    cout << *(rets+1)  << endl;
    cout << *(rets+2)  << endl;
    cout << *(rets+3)  << endl;
    return 0;
}