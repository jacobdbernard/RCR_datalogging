#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <wiringPi.h>

void newLogFile(void){
    std::cout << "exiting" << std::endl;
    system("kill $(ps -e | pgrep candump)"); // kill the candump, need to check if this fucks with logging, hopefully not
    usleep(500000); //sleep for half a second to allow to for file to be saved, duration needs to be adjusted
    system("/home/pi/CAN/RCR_datalogging/candumpExternal can0 -l -s 0"); // candump again, to be 
    sleep(5); // sleep here so that the button press cannot be done again so soon
    
}

int main(int argc, char const *argv[]) {   
        
    system("sudo /sbin/ip link set can0 up type can bitrate 500000"); 
                                      // This is ran initially as logging should start as soon as the pi is on
    system("/home/pi/CAN/RCR_datalogging/candumpExternal can0 -l -s 0"); // candump with the option to log(-l) as well as 
                                      // continue to output to console (-s 0)
    
    wiringPiSetup();
    wiringPiISR(3, INT_EDGE_FALLING, &newLogFile);//3 is the wiringPi pin number

    std::cout <<"Setup Complete" << std:: endl;

    while(true) { //sleeping indefinitely so that the program can stay open and wait for button presses
	    sleep(60);
    }
	
    std::cout <<"exiting" <<std::endl;  //honestly you should never ever get here
    return 0;
}
